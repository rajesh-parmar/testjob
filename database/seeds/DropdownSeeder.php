<?php

use Illuminate\Database\Seeder;
use App\Models\LevelType;
use App\Models\PostalDeliveryType;
use App\Models\State;
use App\Models\StreetType;
use App\Models\StreetSuffixType;
use App\Models\UnitType;

/**
 * Dropdown seeder.
 *
 * Use to seed all required dropdown. For ex. LevelType, UnitType etc.
 */
class DropdownSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

    	\DB::table('level_types')->delete();
        \DB::table('unit_types')->delete();
        \DB::table('street_types')->delete();
        \DB::table('street_suffix_types')->delete();
        \DB::table('states')->delete();

    	//seeding Level types table

        $levelTypes = [
        	'Basement'           => 'B',
        	'Floor'              => 'FL',
        	'Ground floor'       => 'G',
        	'Level'              => 'L',
        	'Lower ground floor' => 'LG',
        	'Mezzanine'          => 'M',
        	'Plaza'              => 'P',
        	'Upper ground floor' => 'UG'
        ];

        foreach ($levelTypes as $levelNm => $levelVal) {

            $level = new LevelType;

            $level->name  = $levelNm;
            $level->value = $levelVal;

        	$level->save();
        }

        // seeding Unit types table

        $unitTypes = [
        	'Apartment'    => 'APT',
        	'Cottage'      => 'CTGE',
        	'Duplex'       => 'DUP',
        	'Factory'      => 'FY',
        	'Flat'         => 'F',
        	'House'        => 'HSE',
        	'Kiosk'        => 'KSK',
        	'Maisonette'   => 'MSNT',
        	'Marine Berth' => 'MB',
        	'Office'       => 'OFF',
        	'Penthouse'    => 'PTHS',
        	'Room'         => 'RM',
        	'Shed'         => 'SHED',
        	'Shop'         => 'SHOP',
        	'Site'         => 'SITE',
        	'Stall'        => 'SL',
        	'Studio'       => 'STU',
        	'Suite'        => 'SE',
        	'Townhouse'    => 'TNHS',
        	'Unit'         => 'U',
        	'Villa'        => 'VLLA',
        	'Ward'         => 'WARD'
        ];

        foreach ($unitTypes as $unitNm => $unitVal) {

            $unitType = new UnitType;

            $unitType->name  = $unitNm;
            $unitType->value = $unitVal;

            $unitType->save();
        }

        //seeding Street types table

        $streetTypes = [
        	'Access'       => 'ACCS',
        	'Alley'        => 'ALLY',
        	'Alleyway'     => 'ALWY',
        	'Amble'        => 'AMBL',
        	'Anchorage'    => 'ANCG',
        	'Approach'     => 'APP',
        	'Arcade'       => 'ARC',
        	'Artery'       => 'ART',
        	'Avenue'       => 'AVE',
        	'Basin'        => 'BASN',
        	'Beach'        => 'BCH',
        	'Bend'         => 'BEND',
        	'Block'        => 'BLK',
        	'Boulevard'	   => 'BVD',
        	'Brace'	       => 'BRCE',
        	'Brae'         => 'BRAE',
        	'Break'        => 'BRK',
        	'Bridge'       => 'BDGE',
        	'Broadway'     => 'BDWY',
        	'Brow'         => 'BROW',
        	'Bypass'       => 'BYPA',
        	'Byway'        => 'BYWY',
        	'Causeway'     => 'CAUS',
        	'Centre'       => 'CTR',
        	'Centreway'    => 'CNWY',
        	'Chase'        => 'CH',
        	'Circle'       => 'CIR',
        	'Circlet'      => 'CLT',
        	'Circuit'      => 'CCT',
        	'Circus'       => 'CRCS',
        	'Close'        => 'CL',
        	'Colonnade'    => 'CLDE',
        	'Common'       => 'CMMN',
        	'Concourse'    => 'CON',
        	'Copse'        => 'CPS',
        	'Corner'       => 'CNR',
        	'Corso'	       => 'CSO',
        	'Court'        => 'CT',
        	'Courtyard'    => 'CTYD',
        	'Cove'         => 'COVE',
        	'Crescent'     => 'CRES',
        	'Crest'        => 'CRST',
        	'Cross'        => 'CRSS',
        	'Crossing'     => 'CRSG',
        	'Crossroad'    => 'CRD',
        	'Crossway'     => 'COWY',
        	'Cruiseway'    => 'CUWY',
        	'Cul-de-sac'   => 'CDS',
        	'Cutting'      => 'CTTG',
        	'Dale'         => 'DALE',
        	'Dell'         => 'DELL',
        	'Deviation'    => 'DEVN',
        	'Dip'          => 'DIP',
        	'Distributor'  => 'DSTR',
        	'Drive'        => 'DR',
        	'Driveway'     => 'DRWY',
        	'Edge'         => 'EDGE',
        	'Elbow'        => 'ELB',
        	'End'          => 'END',
        	'Entrance'     => 'ENT',
        	'Esplanade'    => 'ESP',
        	'Estate'       => 'EST',
        	'Expressway'   => 'EXP',
        	'Extension'    => 'EXTN',
        	'Fairway'      => 'FAWY',
        	'Fire track'   => 'FTRK',
        	'Firetrail'    => 'FITR',
        	'Flat'         => 'FLAT',
        	'Follow'       => 'FOLW',
        	'Footway'      => 'FTWY',
        	'Foreshore'    => 'FSHR',
        	'Formation'    => 'FORM',
        	'Freeway'      => 'FWY',
        	'Front'        => 'FRNT',
        	'Frontage'     => 'FRTG',
        	'Gap'	       => 'GAP',
        	'Garden'       => 'GDN',
        	'Gardens'      => 'GDNS',
        	'Gate'         => 'GTE',
        	'Gates'        => 'GTES',
        	'Glade'        => 'GLD',
        	'Glen'         => 'GLEN',
        	'Grange'       => 'GRA',
        	'Green'        => 'GRN',
        	'Ground'       => 'GRND',
        	'Grove'        => 'GR',
        	'Gully'        => 'GLY',
        	'Heights'      => 'HTS',
        	'Highroad'	   => 'HRD',
        	'Highway'      => 'HWY',
        	'Hill'         => 'HILL',
        	'Interchange'  => 'INTG',
        	'Intersection' => 'INTN',
        	'Junction'     => 'JNC',
        	'Key'          => 'KEY',
        	'Landing'      => 'LDG',
        	'Lane'         => 'LANE',
        	'Laneway'      => 'LNWY',
        	'Lees'         => 'LEES',
        	'Line'         => 'LINE',
        	'Link'         => 'LINK',
        	'Little'	   => 'LT',
        	'Lookout'      => 'LKT',
        	'Loop'         => 'LOOP',
        	'Lower'        => 'LWR',
        	'Mall'         => 'MALL',
        	'Meander'      => 'MNDR',
        	'Mews'         => 'MEWS',
        	'Motorway'     => 'MWY',
        	'Mount'        => 'MT',
        	'Nook'         => 'NOOK',
        	'Outlook'      => 'OTLK',
        	'Parade'       => 'PDE',
        	'Park'         => 'PARK',
        	'Parklands'    => 'PKLD',
        	'Parkway'	   => 'PKWY',
        	'Part'         => 'PART',
        	'Pass'	       => 'PASS',
        	'Path'         => 'PATH',
        	'Pathway'      => 'PHWY',
        	'Piazza'       => 'PIAZ',
        	'Place'        => 'PL',
        	'Plateau'      => 'PLAT',
        	'Plaza'        => 'PLZA',
        	'Pocket'       => 'PKT',
        	'Point'        => 'PNT',
        	'Port'         => 'PORT',
        	'Promenade'    => 'PROM',
        	'Quad'         => 'QUAD',
        	'Quadrangle'   => 'QDGL',
        	'Quadrant'     => 'QDRT',
        	'Quay'         => 'QY',
        	'Quays'        => 'QYS',
        	'Ramble'       => 'RMBL',
        	'Ramp'         => 'RAMP',
        	'Range'        => 'RNGE',
        	'Reach'        => 'RCH',
        	'Reserve'      => 'RES',
        	'Rest'         => 'REST',
        	'Retreat'      => 'RTT',
        	'Ride'         => 'RIDE',
        	'Ridge'        => 'RDGE',
        	'Ridgeway'     => 'RGWY',
        	'Right of Way' => 'ROWY',
        	'Ring'         => 'RING',
        	'Rise'         => 'RISE',
        	'River'        => 'RVR',
        	'Riverway'     => 'RVWY',
        	'Riviera'      => 'RVRA',
        	'Road'         => 'RD',
        	'Roads'        => 'RDS',
        	'Roadside'     => 'RDSD',
        	'Roadway'      => 'RDWY',
        	'Ronde'        => 'RNDE',
        	'Rosebowl'     => 'RSBL',
        	'Rotary'       => 'RTY',
        	'Round'        => 'RND',
        	'Route'        => 'RTE',
        	'Row'          => 'ROW',
        	'Rue'          => 'RUE',
        	'Run'          => 'RUN',
        	'Service Way'  => 'SWY',
        	'Siding'       => 'SDNG',
        	'Slope'        => 'SLPE',
        	'Sound'        => 'SND',
        	'Spur'         => 'SPUR',
        	'Square'       => 'SQ',
        	'Stairs'       => 'STRS',
        	'State Highway'=> 'SHWY',
        	'Steps'        => 'STPS',
        	'Strand'       => 'STRA',
        	'Street'       => 'ST',
        	'Strip'        => 'STRP',
        	'Subway'       => 'SBWY',
        	'Tarn'         => 'TARN',
        	'Terrace'      => 'TCE',
        	'Thoroughfare' => 'THOR',
        	'Toll way'     => 'TLWY',
        	'Top'          => 'TOP',
        	'Tor'          => 'TOR',
        	'Towers'       => 'TWRS',
        	'Track'        => 'TRK',
        	'Trail'        => 'TRL',
        	'Trailer'      => 'TRLR',
        	'Triangle'     => 'TRI',
        	'Trunkway'     => 'TKWY',
        	'Turn'         => 'TURN',
        	'Underpass'    => 'UPAS',
        	'Upper'        => 'UPR',
        	'Vale'         => 'VALE',
        	'Viaduct'      => 'VDCT',
        	'View'         => 'VIEW',
        	'Villas'       => 'VLLS',
        	'Vista'        => 'VSTA',
        	'Wade'         => 'WADE',
        	'Walk'         => 'WALK',
        	'Walkway'      => 'WKWY',
        	'Way'          => 'WAY',
        	'Wharf'        => 'WHRF',
        	'Wynd'         => 'WYND',
        	'Yard'         => 'YARD'
        ];

        foreach ($streetTypes as $streetNm => $streetVal) {

            $streetType = new StreetType;

            $streetType->name  = $streetNm;
            $streetType->value = $streetVal;

            $streetType->save();
        }


        // Seeding Street suffix types table

        $streetSuffix = [
        	'Central'    =>	'CN',
        	'East'       =>	'E',
        	'Extension'  =>	'EX',
        	'Lower'      =>	'LR',
        	'North'      => 'N',
        	'North East' =>	'NE',
        	'North West' =>	'NW',
        	'South'      =>	'S',
        	'South East' =>	'SE',
        	'South West' =>	'SW',
        	'Upper'      => 'UP',
        	'West'       => 'W'
        ];

        foreach ($streetSuffix as $suffixNm => $suffixVal) {

            $suffixType = new StreetSuffixType;

            $suffixType->name  = $suffixNm;
            $suffixType->value = $suffixVal;

            $suffixType->save();
        }

        // Seeding Postal delivery types table

        $postalType = [
        	'Care-of Post Office (Poste Restante)' => 'CARE PO',
        	'Community Mail Agent'                 => 'CMA',
        	'Community Mail Bag'                   => 'CMB',
        	'General Post Office Box'              => 'GPO BOX',
        	'Locked Mail Bag Service'              => 'LOCKED BAG',
        	'Mail Service'                         => 'MS',
        	'Post Office Box'                      => 'PO BOX',
        	'Private Mail Bag Service'             => 'PRIVATE BAG',
        	'Roadside Delivery'                    => 'RSD',
        	'Roadside Mail Box/Bag'                => 'RMB',
        	'Roadside Mail Service'                => 'RMS'
        ];

        foreach ($postalType as $postalNm => $postalVal) {

            $postal = new PostalDeliveryType;

            $postal->name  = $postalNm;
            $postal->value = $postalVal;

            $postal->save();
        }


        // Seeding States table

        $states = [
        	'New South Wales'              => 'NSW',
        	'Victoria'                     => 'VIC',
        	'Australian Capital Territory' => 'ACT',
        	'Queensland'                   => 'QLD',
        	'South Australia'              => 'SA',
        	'Western Australia'            => 'WA',
        	'Tasmania'                     => 'TAS',
        	'Northern Territory'           => 'NT'
        ];

        foreach ($states as $stateNm => $stateVal) {

            $state = new State;

            $state->name  = $stateNm;
            $state->value = $stateVal;

            $state->save();
        }

    }
}
