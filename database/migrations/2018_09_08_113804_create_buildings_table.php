<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuildingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buildings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('unit_type', 10)->nullable();
            $table->string('unit_number', 5)->nullable();
            $table->string('level_type', 10)->nullable();
            $table->string('level_number', 5)->nullable();
            $table->string('street_number', 10)->nullable();
            $table->string('street_number_to', 10)->nullable();
            $table->string('street_name', 50)->nullable();
            $table->string('street_type', 10)->nullable();
            $table->string('street_suffix', 10)->nullable();
            $table->string('postal_delivery_type', 10)->nullable();
            $table->string('postal_delivery_number', 10)->nullable();
            $table->string('building_site_name', 50)->nullable();
            $table->string('suburb', 50)->nullable();
            $table->string('postcode', 4);
            $table->string('state', 10);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('unit_type')->references('value')->on('unit_types');
            $table->foreign('level_type')->references('value')->on('level_types');
            $table->foreign('street_type')->references('value')->on('street_types');
            $table->foreign('street_suffix')->references('value')->on('street_suffix_types');
            $table->foreign('postal_delivery_type')->references('value')->on('postal_delivery_types');
            $table->foreign('state')->references('value')->on('states');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buildings');
    }
}
