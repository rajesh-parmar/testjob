$(document).ready(function(){
  $.validator.addMethod("custom_alphanumeric", function(value, element) {
      return this.optional(element) || /^[a-zA-Z0-9\s]+$/.test(value);
  }, "Letters, numbers, and underscores only please");

  $.validator.addMethod( "alphanumeric", function( value, element ) {
    return this.optional( element ) || /^\w+$/i.test( value );
  }, "Letters, numbers, and underscores only please" );

  $('#buildingForm').validate({
    rules:{
      unitType : {
        'required' : {
          depends: function(element) {
            if($('#unitNumber').val()!=''){
              return true;
            } else {
              return false;
            }
          }
        }
      },
      unitNumber : {
        'required' : {
          depends: function(element) {
            if($('#unitType').val()!=''){
              return true;
            } else {
              return false;
            }
          }
        },
        maxlength: 5,
        alphanumeric: true
      },
      levelType : {
        'required' : {
          depends: function(element) {
            if($('#levelNumber').val()!=''){
              return true;
            } else {
              return false;
            }
          }
        }
      },
      levelNumber : {
        'required' : {
          depends: function(element) {
            if($('#levelType').val()!=''){
              return true;
            } else {
              return false;
            }
          }
        },
        maxlength: 5,
        alphanumeric: true
      },
      streetNumber : {
        'required' : {
          depends: function(element) {
            if($('#streetNumberTo').val()!=''){
              return true;
            } else {
              return false;
            }
          }
        },
        maxlength: 10,
        alphanumeric: true
      },
      streetNumberTo : {
        maxlength: 10,
        alphanumeric: true
      },
      streetType : {
        'required' : {
          depends: function(element) {
            if($('#streetName').val()!=''){
              return true;
            } else {
              return false;
            }
          }
        }
      },
      streetName : {
        minlength: 2,
        maxlength: 50,
        custom_alphanumeric: true
      },
      postalDeliveryType : {
        'required' : {
          depends: function(element) {
            if($('#postalDeliveryNumber').val()!=''){
              return true;
            } else {
              return false;
            }
          }
        }
      },
      postalDeliveryNumber : {
        'required' : {
          depends: function(element) {
            if($('#postalDeliveryType').val()!=''){
              return true;
            } else {
              return false;
            }
          }
        },
        maxlength: 5,
        alphanumeric: true
      },
      buildingName: {
        maxlength: 50,
        custom_alphanumeric: true
      },
      suburb: {
        'required': true,
        minlength: 3,
        maxlength: 50,
        custom_alphanumeric: true
      },
      postcode: {
        'required' : true,
        minlength: 4,
        maxlength: 4,
        number: true
      },
      state: {
        "required": true
      }
    },
    messages: {
      unitType: {
        'required': "Please select unit type",
      },
      unitNumber: {
        'required': "Please enter unit number",
      },
      levelType: {
        'required': "Please select level type",
      },
      levelNumber: {
        'required': "Please enter level number",
      },
      streetNumber:{
        'required' : "Please enter street number"
      },
      streetNumberTo: {
        'required' : "Please enter street number to"
      },
      streetType:{
        'required' : "Please select street type"
      },
      streetName: {
        'required' : "Please enter street name",
      },
      postalDeliveryType: {
        'required' : "Please select postal delivery type"
      },
      postalDeliveryNumber: {
        'required' : "Please enter postal delivery number"
      },
      suburb : {
        'required' : "Please enter suburb"
      },
      postcode: {
        'required' : "Please enter postcode"
      },
      state : {
        'required' : "Please select state"
      }

    }
  });

  $('#save_building').click(function() {

    if($('#buildingForm').valid()){
      $.ajax({
        method: 'POST',
        url: 'save',
        data: $('#buildingForm').serializeArray(),
        dataType: 'json',
        headers: {
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function(data) {
          if(data.success == false) {
            var errors = '';
            $.each( data.messages, function( index, value ) {
              errors += `<p>` + value + `</p>`;
            });

            $('div.errors').html(errors);
            $('div.errors').show();
          } else {
            location.href = 'success/' + data.data.buildingId;
          }
        },
        error: function(err) {
          console.log(err);
        }
      });
    }
  });
});