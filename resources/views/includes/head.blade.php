<head>
	<title>Test Job{{ isset($title) ? ' - '. $title : ''}}</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">

	  <!-- Bootstap -->
  <link rel="stylesheet" type="text/css" href="{{ asset('plugins/bootstrap-4.0/css/bootstrap.min.css') }}">

  <!-- Custom css -->
  <link rel="stylesheet" href="{{ asset('css/custom.css') }}">

</head>