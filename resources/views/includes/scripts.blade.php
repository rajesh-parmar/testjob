<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery and Popper-->
<script type="text/javascript" src="{{ asset('plugins/jquery/jquery-3.3.1.min.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/jquery/popper.min.js') }}"></script>

<!-- Bootstrap 4.0 -->
<script type="text/javascript" src="{{ asset('plugins/bootstrap-4.0/js/bootstrap.min.js') }}"></script>