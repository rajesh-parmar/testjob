<!DOCTYPE html>
<html>
	@include('includes.head')
<body>
	<div class="jumbotron text-lg-center" style="height: 100vh;">
	  <h1 class="display-3" style="margin-top: 50px;">Thank You!</h1>
	  <p class="lead"><span>Buiding added successfully!</span> Your building id is: <span style="font-weight: bold;">{{ $buildingId }}</span>
	  </p>

		<hr>
	  <p class="lead">
	    <a class="btn btn-success btn-sm" href="{{ url('/buildings/new') }}" role="button" style="width: 80px;">Add more</a>
	  </p>
	</div>

	@include('includes.scripts')
</body>
</html>