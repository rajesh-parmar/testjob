<!DOCTYPE html>
<html>
  @include('includes.head')
  <body>

    <div class="container">
      <h1 class="page-title text-center">Add Building</h1>

        <form action="" name="buildingForm" id="buildingForm" autocomplete="off">

          <div class="errors text-center" style="color:red;display: none;">
          </div>

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="unitType">Unit Type</label>
                <select class="form-control" id="unitType" name="unitType">
                  <option value="">Select Unit Type</option>
                  @if($unitTypes)
                    @foreach($unitTypes as $unitType)
                      <option value="{{ $unitType->value }}">{{ $unitType->name }}</option>
                    @endforeach
                  @endif
                </select>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
                <label for="unitNumber">Unit Number</label>
                <input type="text" class="form-control" id="unitNumber" placeholder="Enter unit number" name="unitNumber">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="levelType">Level Type</label>
                <select class="form-control" id="levelType" name="levelType">
                  <option value="">Select Level Type</option>
                  @if($levelTypes)
                    @foreach($levelTypes as $levelType)
                      <option value="{{ $levelType->value }}">{{ $levelType->name }}</option>
                    @endforeach
                  @endif
                </select>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
                <label for="levelNumber">Level Number</label>
                <input type="text" class="form-control" id="levelNumber" placeholder="Enter level number" name="levelNumber">
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="streetNumber">Street Number</label>
                <input type="text" class="form-control" id="streetNumber" placeholder="Enter street number" name="streetNumber">
              </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
                <label for="streetNumberTo">Street Number To</label>
                <input type="text" class="form-control" id="streetNumberTo" placeholder="Enter street number to" name="streetNumberTo">
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="streetName">Street Name</label>
            <input type="text" class="form-control" id="streetName" placeholder="Enter street name" name="streetName">
          </div>

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="streetType">Street Type</label>
                <select class="form-control" id="streetType" name="streetType">
                  <option value="">Select Street Type</option>
                  @if($streetTypes)
                    @foreach($streetTypes as $streetType)
                      <option value="{{ $streetType->value }}">{{ $streetType->name }}</option>
                    @endforeach
                  @endif
                </select>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
                <label for="streetSuffix">Street Suffix</label>
                <select class="form-control" id="streetSuffix" name="streetSuffix">
                  <option value="">Select Street Suffix</option>
                  @if($streetSuffix)
                    @foreach($streetSuffix as $suffix)
                      <option value="{{ $suffix->value }}">{{ $suffix->name }}</option>
                    @endforeach
                  @endif
                </select>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-lg-6">
              <div class="form-group">
                <label for="postalDeliveryType">Postal Delivery Type</label>
                <select class="form-control" id="postalDeliveryType" name="postalDeliveryType">
                  <option value="">Select Postal Delivery Type</option>
                  @if($postalTypes)
                    @foreach($postalTypes as $postalType)
                      <option value="{{ $postalType->value }}">{{ $postalType->name }}</option>
                    @endforeach
                  @endif
                </select>
              </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
                <label for="postalDeliveryNumber">Postal Delivery Number</label>
                <input type="text" class="form-control" id="postalDeliveryNumber" placeholder="Enter postal delivery number" name="postalDeliveryNumber">
              </div>
            </div>
          </div>

          <div class="form-group">
            <label for="buildingName">Building Site Name</label>
            <input type="text" class="form-control" id="buildingName" placeholder="Enter building site name" name="buildingName">
          </div>

          <div class="form-group">
            <label for="suburb">Suburb</label>
            <input type="text" class="form-control" id="suburb" placeholder="Enter suburb" name="suburb">
          </div>

          <div class="row">

            <div class="col-lg-6">
              <div class="form-group">
                <label for="postcode">Postcode</label>
                <input type="text" class="form-control" id="postcode" placeholder="Enter postcode" name="postcode">
              </div>
            </div>

            <div class="col-lg-6">
              <div class="form-group">
                <label for="state">State</label>
                <select class="form-control" id="state" name="state">
                  <option value="">Select State</option>
                  @if($states)
                    @foreach($states as $state)
                      <option value="{{ $state->value }}">{{ $state->name }}</option>
                    @endforeach
                  @endif
                </select>
              </div>
            </div>

          </div>

          <div class="form-group" style="text-align: center;margin-top: 10px;">
            <button type="button" id="save_building" class="btn btn-success" style="width: 100px;">Save</button>
          </div>

        </form>

    </div>

    @include('includes.scripts')

    <!-- jQuery Validation -->
    <script type="text/javascript" src="{{ asset('plugins/jquery-validation/jquery.validate.min.js') }}"></script>
    <!-- <script type="text/javascript" src="{{ asset('plugins/jquery-validation/additional-methods.min.js') }}"></script> -->

    <!-- Custom js -->
    <script type="text/javascript" src="{{ asset('js/custom.js') }}"></script>
  </body>
</html>
