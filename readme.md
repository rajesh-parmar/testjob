# Test Job

This is just a test job to add building in database.

## Prerequisites

* [Composer](https://getcomposer.org/)
* [Laravel 5.7](http://laravel.com/)
* [MySQL](https://www.mysql.com/downloads/)

## Installation

Clone project

```
git clone https://gitlab.com/rajesh-parmar/testjob.git
```

Change Directory

```
cd testjob
```

Install all dependencies

```
composer install
```

Create a .env file

```
Create a `.env` file in root of the prject as per `.env.example` supplied.
```

Generate application key

```
php artisan key:generate
```

Create database `buildings` and run below command:

```
php artisan migrate
```

Run dropdown seeder to fill dropdown reference table in database. To do that run following command

```
php artisan db:seed
```

## Testing

Go to `Home page` and click on `ADD BUILDING` link OR go to `buildings/new` page to add new building.