<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'httpLogger'], function ($url) {

	Route::get('buildings/new', 'BuildingController@newBuilding');
	Route::post('buildings/save', 'BuildingController@store');
	Route::get('buildings/success/{building}', 'BuildingController@success');

});

