<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Building extends Model
{
	use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

 	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'unit_type',
    	'unit_number',
    	'level_type',
    	'level_number',
    	'street_number',
    	'street_number_to',
    	'street_name',
    	'street_type',
    	'street_suffix',
    	'postal_delivery_type',
    	'postal_delivery_number',
    	'building_site_name',
    	'suburb',
    	'postcode',
    	'state',
    	'created_at'
    ];   
}
