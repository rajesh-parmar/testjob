<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ErrorLog extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'request_url',
    	'data',
    	'errors',
    	'user_agent',
    	'ip_address',
    ];
}
