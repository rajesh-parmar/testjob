<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PostalDeliveryType extends Model
{
	protected $primaryKey = "value";

	public $incrementing  = false;

	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'value',
    ];
}
