<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Building;
use App\Models\UnitType;
use App\Models\LevelType;
use App\Models\StreetType;
use App\Models\StreetSuffixType;
use App\Models\PostalDeliveryType;
use App\Models\State;
use Validator;
use App\Http\Helpers\TrimRequestData;
use App\Http\Helpers\Logger;
use Hashids;
use Log;

/**
 * Building controller.
 *
 * To create a new building.
 */
class BuildingController extends Controller
{
    public function newBuilding(Request $request) {

        $unitTypes    = UnitType::all();
        $levelTypes   = LevelType::all();
        $streetTypes  = StreetType::all();
        $streetSuffix = StreetSuffixType::all();
        $postalTypes  = PostalDeliveryType::all();
        $states       = State::all();

        return view('buildingForm', [
            'unitTypes'    => $unitTypes,
            'levelTypes'   => $levelTypes,
            'streetTypes'  => $streetTypes,
            'streetSuffix' => $streetSuffix,
            'postalTypes'  => $postalTypes,
            'states'       => $states,
            'title'        => 'Add Building'
        ]);

    }

    public function store(Request $request) {

        // Trim Request data
        $data = TrimRequestData::trim($request->all());

        $rules = [
            'unitType'             => 'required_with:unitNumber',
            'unitNumber'           => 'required_with:unitType|nullable|alpha_num|min:1|max:5',
            'levelType'            => 'required_with:levelNumber',
            'levelNumber'          => 'required_with:levelType|nullable|alpha_num|min:1|max:5',
            'streetNumber'         => 'required_with:streetNumberTo|nullable|alpha_num|min:1|max:10',
            'streetNumberTo'       => 'min:1|alpha_num|max:10',
            'streetName'           => 'min:2|regex:/^[a-zA-Z0-9\s]+$/|max:50',
            'streetType'           => 'required_with:streetName|nullable',
            'postalDeliveryType'   => 'required_with:postalDeliveryNumber',
            'postalDeliveryNumber' => 'required_with:postalDeliveryType|nullable|alpha_num|min:1|max:10',
            'buildingName'         => 'nullable|regex:/^[a-zA-Z0-9\s]+$/|max:50',
            'suburb'               => 'required|min:3|regex:/^[a-zA-Z0-9\s]+$/|max:50',
            'postcode'             => 'required|digits:4|numeric',
            'state'                => 'required'
        ];

        $validator = Validator::make($data, $rules, []);

        Log::debug("BuildingController->store() - Adding a business...", [
            'data' => $data
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();

            // save error logs in database
            Logger::error($request, $messages);

            return response()->json(['success' => false, 'data' => null, 'messages' => $messages]);
        }


        // Save building

        $building = new Building;

        $building->unit_type              = $data['unitType']?:NULL;
        $building->unit_number            = $data['unitNumber']?:NULL;
        $building->level_type             = $data['levelType']?:NULL;
        $building->level_number           = $data['levelNumber']?:NULL;
        $building->street_number          = $data['streetNumber']?:NULL;
        $building->street_number_to       = $data['streetNumberTo']?:NULL;
        $building->street_suffix          = $data['streetSuffix']?:NULL;
        $building->street_type            = $data['streetType']?:NULL;
        $building->street_name            = $data['streetName']?:NULL;
        $building->street_number          = $data['streetNumber']?:NULL;
        $building->postal_delivery_type   = $data['postalDeliveryType']?:NULL;
        $building->postal_delivery_number = $data['postalDeliveryNumber']?:NULL;
        $building->building_site_name     = $data['buildingName']?:NULL;
        $building->suburb                 = $data['suburb'];
        $building->postcode               = $data['postcode'];
        $building->state                  = $data['state'];

        $building->save();

        Log::info("Building added successfully!", [
            'building_id' => $building->id
        ]);

        $content = [
            'success' => true,
            'data'    => [
                'buildingId' => Hashids::encode($building->id) // encode building id
            ],
            'message' => "Building added successfully!"
        ];

        return response()->json($content);

    }

    public function success(Request $request, $building) {

        return view('thankyou', ['buildingId' => $building, 'title' => 'Thankyou']);
    }
}
