<?php

namespace App\Http\Helpers;

use App\Models\ErrorLog;

class Logger
{
    /**
     * Save error logs in database
     *
     * @param $request Request
     *
     * @param $errors array
     *
     * @author Rajesh Parmar
     */
    public static function error($request, $errors)
    {
        $log = new ErrorLog;

        $log->request_url = $request->fullUrl();
        $log->data        = http_build_query($request->all());
        $log->errors      = $errors;
        $log->user_agent  = $request->server('HTTP_USER_AGENT');
        $log->ip_address  = $request->getClientIp();
        $log->save();
    }

}
