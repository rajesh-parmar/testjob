<?php

namespace App\Http\Helpers;

class TrimRequestData
{
    /**
     * Remove whitespaces, backslashes and convert predefined characters to HTML entities from request data
     *
     * @param $data array
     *
     * @return $data array
     *
     * @author Rajesh Parmar
     */
    public static function trim($data)
    {

        foreach ($data as $key => $value) {
            $value = trim($value);
            $value = stripslashes($value);
            $value = htmlspecialchars($value);

            $data[$key] = $value;
        }

        return $data;
    }

}
