<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\EventLog;

class HttpLogger
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $log = new EventLog;

        $log->request_url = $request->fullUrl();
        $log->data        = http_build_query($request->all());
        $log->user_agent  = $request->server('HTTP_USER_AGENT');
        $log->ip_address  = $request->getClientIp();
        $log->save();

        return $next($request);
    }
}
